# podcast-chapter-txt2json

A simple Python script that converts plain podcast-chapter-texfiles in JSON-files.

### Usage

    python chapters2json.py file1 file 2 file3 ...`

or via stdin e.g.

    ls chapters*.txt | python chapters2json.py

The script convert the text-format to a JSON-format.

An example:

podcast-episode-72-chapters.txt:

    00:00:00.000 The great intro <https://herrthees.de>
    00:00:41.000 Was Terry Pratchett the greatest author ever? <https://en.wikipedia.org/wiki/Terry_Pratchett>
    00:11:51.000 HAL - a psychopathic computer
    00:23:44.000 That's all folks! <http://endoftheinternet.com/>

podcast-episode-72-chapters.json:

    [{"start": "00:00:00.000", "title": "The great intro", "href": "https://herrthees.de"}, 
    {"start": "00:00:41.000", "title": "Was Terry Pratchett the greatest author ever?", "href": "https://en.wikipedia.org/wiki/Terry_Pratchett"},
    {"start": "00:11:51.000", "title": "HAL - a psychopathic computer", "href": ""}, 
    {"start": "00:23:44.000", "title": "That's all folks!", "href": "http://endoftheinternet.com/"}]