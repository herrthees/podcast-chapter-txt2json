import json
import sys
import os
from pathlib import Path
import re

chapterFileList = []

# Inputfiles from stdin
if not os.isatty(0): 
    for d in sys.stdin:
        chapterFileList.append(d.strip())
# Inputfiles from commandline arguments
else:
    for d in sys.argv:
        chapterFileList.append(d)
    chapterFileList = chapterFileList[1:]

if len(chapterFileList) == 0:
    print('No input file(s).\nUse a list of chapter textfiles as arguments oder via the standard input (stdin).')
    exit()

for textFile in chapterFileList:
    jsonData = []
    with open(textFile, 'r') as f:
        p = Path(textFile)
        href = ''
        for line in f:
            textData = line.split(' ', 1) # Split after first space
            start = textData[0]
            titleUrlGroup = re.match('^(.*) <(.*)>$', textData[1].strip()) # Match regex chapter-title and href between < >

            if titleUrlGroup is None:
                title = textData[1].strip()
                href = ''
            else:
                title = titleUrlGroup.groups()[0]
                href = titleUrlGroup.groups()[1]

            jsonData.append({
                'start': start,
                'title': title,
                'href': href
            })

        jsonFile = str(p.with_suffix('.json')) #replace file extension with .json
    with open(jsonFile, 'w') as f:
        json.dump(jsonData, f)